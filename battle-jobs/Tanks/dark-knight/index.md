---
tags:
    - DRK
---
# Dark Knight

## What is a Dark Knight?

The pious Ishgardian clergy guide the flock, and the devout knights protect the weak. Yet even the holiest of men succumb to the darkest of temptations. These sentinels bear no shields declaring their allegiance. Instead, their greatswords act as beacons to guide the meek through darkness.

Dark knight ({{ MCH }}) is a Tank.

Dark Knight uses a great sword as their weapon.

## How to unlock Dark Knight

1. Get to Ishgard in the Heavensward Expansion
2. Start the quest [Our End](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/92c86ebbdc7/) in Foundation
3. Dark Knight starts at level 30.

!!! info "Unlock info"
    You NEED to have finished the whole of {{ ARR }}, including patch content to be able to unlock it.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("eByfSznEkCU")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ MCH }}
