---
tags:
    - PLD
    - GLD
---
# Paladin

## What is a Paladin?

To be a paladin is to protect, and those who choose to walk this path will become the iron foundation upon which the party's defense is built.

Paladin ({{ PLD }}) is a tank.

Paladin uses a shield and a sword as their weapon.

## How to unlock Paladin

1. Unlock Gladiator in Gridania, which starts at level 1 ([Way of the Gladiator](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/18894b76889/))
2. Level up Gladiator to level 30
3. Unlock {{ PLD }} through doing the job quests. ([Paladin's Pledge](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/90a79bf83dc/))

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("_VVwWezk-Y4")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ PLD }}
