---
tags:
    - GNB
---
# Gunbreaker

## What is a Gunbreaker?

The Hrothgar of northern Ilsabard have passed the art of the gunblade from one generation to the next. The weapon itself combines a sword with a firing mechanism, emitting a range of magical effects by utilizing aetherially imbued cartridges

Gunbreaker ({{ GNB }}) is a Tank.

Gunbreaker uses a gunblade as their weapon.

## How to unlock Gunbreaker

1. Own the Shadowbringers Expansion
2. Level any other Job to level 60
3. Start the quest [The Makings of a Gunbreaker](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/e3f6c6209c3/) in Limsa Lominsa
4. Gunbreaker starts at level 60.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("PlxDn7S4ruI")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ GNB }}
