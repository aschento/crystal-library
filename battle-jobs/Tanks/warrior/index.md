---
tags:
    - WAR
    - MRD
---
# Warrior

## What is a Warrior?

Wielding greataxes and known as warriors, these men and women learn to harness their inner-beasts and translate that power to unbridled savagery on the battlefield.

Warrior ({{ WAR }}) is a tank.

Warrior uses a great axe as their weapon.

## How to unlock Warrior

1. Unlock Marauder in Gridania, which starts at level 1 ([Way of the Marauder](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/66d611f22e0/))
2. Level up Marauder to level 30
3. Unlock {{ WAR }} through doing the job quests. ([Pride and Duty (Will Take You from the Mountain)](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/75e8ccec1f4/))

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("UDUzsguKt-I")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ WAR }}
