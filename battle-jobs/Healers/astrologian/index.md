---
tags:
    - AST
---
# Astrologian

## What is an Astrologian?

Thus was astromancy born—a new form of magick which grants its users power over fate.

Astrologian ({{ AST }}) is a flex healer. {{ AST }} is able to either be a shield healer (nocturnal sect), or a pure healer(diurnal sect), depending on the sect selected. It's also the only dps to have a damage boost ability for the whole party, using its cards

Astrolgian uses a Star Globe as their weapon.

## How to unlock Astrologian

1. Get to Ishgard in the Heavensward Expansion
2. Start the quest [Stairway to the Heavens](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/6b00e8264b7/)
3. Astrologian starts at level 30

!!! info "Unlock info"
    You NEED to have finished the whole of {{ ARR }}, including patch content to be able to unlock it.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("pomcD5GYJ8Q")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ AST }}
