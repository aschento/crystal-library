---
tags:
    - WHM
    - CNJ
---
# White Mage

## What is a White Mage?

Those who would walk the path of the white mage are healers without peer, possessed of the power to deliver comrades from the direst of afflictions—even the icy grip of death itself.

White Mage ({{ WHM }}) is a pure healer. Of the Healers, White Mage focuses on reactive healing, while trying to maximize damage output done by themselves.

White Mage uses a two handed staff as their weapon.

## How to unlock White Mage

1. Unlock Conjurer in Gridania, which starts at level 1 ([Way of the Conjurer](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/b4758d83d19/))
2. Level up Conjurer to level 30
3. Unlock {{ WHM }} through doing the job quests. ([Seer Folly](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/e3eba90aefc/))

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](Advancement Job Quest does not appear](../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("BX6adKlGXNc")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ WHM }}
