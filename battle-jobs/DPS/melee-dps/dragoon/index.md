---
tags:
    - DRG
    - LNC
---
# Dragoon

## What is a Dragoon?

Dragoons, Born amidst the timeless conflict between men and dragons, these lance-wielding knights have developed an aerial style of combat, that they might better pierce the scaled hides of their mortal foes.

Dragoon ({{ DRG }}) is a Melee DPS which is highly movable (lots of jumps back and forth).

Dragoon uses a Lance as their weapon.

## How to unlock Dragoon

1. Unlock Lancer in Old Gridania, which starts at level 1 ([Way of the Lancer](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/f5a62b54de4/))
2. Level up Lancer to level 30
3. Unlock {{ DRG }} through doing the job quests. ([Eye of the Dragon](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/0533230f632/))

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("_S-UuGmLPe4")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ DRG }}
