---
tags:
    - BRD
    - ARC
---
# Bard

## What is a Bard?

Bards trace their origins back to the bowmen of eld, who sang in the heat of battle to fortify the spirits of their companions.
In time, their impassioned songs came to hold sway over the hearts of men, inspiring their comrades to great feats and granting peace unto those who lay upon the precipice of death.

Bard ({{ BRD }}) is a Physical Ranged DPS which uses songs to empower other players.

Bard uses a bow as their weapon.

## How to unlock Bard

1. Unlock Archer in New Gridania, which starts at level 1 ([Way of the Archer](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/0754ced391b/))
2. Level up Archer to level 30
3. Unlock {{ BRD }} through doing the job quests. ([A Song of Bards and Bowmen](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/e8c7133b92a/))

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("MVPkubT--qg")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ BRD }}
