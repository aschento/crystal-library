---
tags:
    - MCH
---
# Machinist

## What is a Machinist?

Following the example of Cid Garlond, who has demonstrated the potency of magitek, the Skysteel Manufactory works tirelessly on the development of advanced armaments. As new and devastating weapons are brought to the fray, a new class of champion arises to wield them―the machinist.

Machinist ({{ MCH }}) is a Physical Ranged DPS which is highly mobile. Contrary to the other two physical DPS, this one focuses more on doing damage, than giving buffs.

Machinist uses a gun as their weapon.

## How to unlock Machinist

1. Get to Ishgard in the Heavensward Expansion
2. Start the quest [Savior of Skysteel](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/8b4a18330a8/) in Foundation
3. Machinist starts at level 30

!!! info "Unlock info"
    You NEED to have finished the whole of {{ ARR }}, including patch content to be able to unlock it.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("wlsdao9KlCU")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv)

## Common asked questions about {{ MCH }}
