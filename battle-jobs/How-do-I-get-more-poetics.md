---
tags:
- Poetics
- Tomestones
- Gearing
---
# How do I get more Poetics?

## Short Answer

There are many ways to get more poetics. One of the most common ways is via daily Duty Roulettes. MSQ Roulette gives 500-600, Alliance Raid Roulette gives 150+, 50/60/70 Roulette gives 120+, and Normal Raid Roulette gives 90+. Additionally, all level 50, 60 or 70 content will give poetics as a reward for duty completion as well as bonus poetics whenever there is a first timer in your party (including yourself). If you are done with dailies and still wish to farm, the Aetherochemical Research Facilty dungeon gives 150 per run. If you are not far enough to have that unlocked, Syrcus Tower gives 100 per run and Castrum Meridianum gives 200 per run.

## Longer Answer

!!! note Amount Obtained  in Short Answer
    Total listed above in the Short Answer section for roulettes are based on the daily roulette bonus *and* the minimum possible recieved from a duty within that roulette. Hence the "+" indicating that, depending on the duty and/or if you have a new person in your group, you may recieve more.

To have the listed roulettes you must have at least 2 duties within their list unlocked. For Alliance Raid Roulette, you can begin the Crystal Tower questline which you need to do anyway before you can get into Heavensward. For 50/60/70 Roulette, there are numerous level 50 optional dungeons you can begin to unlock once you see the first set of credits roll at the end of main A Realm Reborn (Many unlock quests for these are found in Mor Dhona). For Normal Raid Roulette, you will need to wait until after main Heavenward when you can begin the Alexander raids as the Normal Raids in A Realm Reborn, the Bahamut Coils, or deemed too difficult to be part of a roulette.

## Extra Info

One other particularly good way of farming poetics, though doing so efficiently would likely require joining a dedicated discord and/or Linkshell is "The Hunt" aka just "Hunts". All A and S Rank hunts give poetics for participating. The reason this is not listed above is that few, if any, organized hunt groups do Hunts for A Realm Reborn and Heavensward and there are not all that many doing Stormblood Hunts either. So while it can be a great source, you may find it unavailable or impractical to do before Stormblood or Shadowbringers.

## See Also

* [What do I spend my Poetics on?](What-do-I-spend-poetics-on.md)
