# Battle Jobs

In this category you can find all information regarding FFXIV Battle jobs, including how to unlock, questions, and general gameplay content.

## Most often asked questions

* [I am Level 30 why no job advancements](I-am-level-30-why-no-job-advancement-quest.md)
* [Why minimize Battle Macros usage](Why-minimize-Battle-Macros-usage.md)
