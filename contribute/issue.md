# How to report an issue

When you are reporting an issue please use the following template:

```md
Page URL:
Issue:
Steps to reproduce:
```

You can report an issue through the following methods:

1. Open an issue on the gitlab repository found here: [https://gitlab.com/Robinaite/ffxiv-faq/-/issues](https://gitlab.com/Robinaite/ffxiv-faq/-/issues).
2. Report it to Robinaite on twitter (either tweet at me, or DM). [Robinaite Twitter](https://twitter.com/robinaite)
3. Fix the issue yourself! Read below on How to contribute to find out more!
