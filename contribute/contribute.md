# How to Contribute

There are two ways to contribute to the project:

1. Updating pages and adding more information
2. Adding new questions and answers or guides.
3. Too complicated? Write the questions and answers in a Google Document, and send them to me on twitter! [Robinaite Twitter](https://twitter.com/Robinaite)

## I want to fix an issue on a page or add more information to an existing page

Pre-requisites:  

- Having a [Gitlab](https://gitlab.com/) account.

Follow the following steps:

1. Go to the page with the issue.
2. Click the edit button next to the title
3. It will redirect you to gitlab
4. Click on the edit button
    1. If its your first time editing the project, it will indicate "You can’t edit files directly in this project. Fork this project and submit a merge request with your changes." click Fork.
5. Edit the texts with the issue. Please make sure you follow the guidelines found here: [Guidelines](guidelines.md)
6. When finished, scroll down and update the Commit Message with a small description of what you changed.
7. Click Commit changes
8. It will load the page to make a new Merge Request.
    1. Update title and description if you want (not necessary)
9. Scroll down and click "Create Merge Request"
10. Now you wait for someone with access to the repository to review your changes and merge (update) them in the project. This can take up to 24 hours.

*If you are knowledgeable with git, feel free to use git to create a merge request, you do not need to follow the steps above.*

## I want to add a new question or page to the website

Pre-requisites:  

- Having a [Gitlab](https://gitlab.com/) account.

Follow the following steps:

1. Go to the gitlab repository found here: [https://gitlab.com/Robinaite/ffxiv-faq](https://gitlab.com/Robinaite/ffxiv-faq)
2. Find the "Web IDE" button and click it.
    1. If its your first time contributing to the project, a message will pop-up saying "You can’t edit files directly in this project. Fork this project and submit a merge request with your changes." Click Fork Project.
3. On the left you will see the folder structure, hover over the folder you want to add a new file to.
4. Click the 3 dots and the arrow downwards to show a dropdown
5. Click "New File".
    1. If you have a file you want to upload yourself, click upload file.
    2. If you want to add a new directior, click "New Directory"
6. Make sure to follow the guidelines on adding new files! You can find the guidelines here: [Guidelines](guidelines.md)
7. When finished click Commit at the bottom.
8. Write a commit message on what you added to the project.
9. Select option "Create new branch"
10. Have option "Start new merge request" enabled
11. It will load the page to make a new Merge Request.
    1. Update title and description if you want (not necessary)
12. Scroll down and click "Create Merge Request"
13. Now you wait for someone with access to the repository to review your changes and merge (update) them in the project. This can take up to 24 hours.

*If you are knowledgeable with git, feel free to use git to create a merge request, you do not need to follow the steps above.*

## Add abreviations

Below you see what you need to do to add abreviations:

1. Add the abreviation to the glossary.yml file. test
2. Add the abreviation to the [Glossary](../glossary/index.md) page
