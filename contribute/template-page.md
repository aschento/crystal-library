# Template Page

Here you can find various templates for pages to use as a base when writing your questions and answers. Make sure you've read the [Guidelines](guidelines.md) for more information on how to write these pages.

You can find more information on how to contribute on [Contribute](index.md) page.

??? info "Questions & Answer Template"
    ```md
    ---
    tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Main tag # this tag should be unique, consisting of the smallest amount of words possible.
    - secondary tag 1 (optional)
    - secondary tag 2 (optional)
    - secondary tag 3 (optional)
    - secondary tag 4 (optional)
    - Secondary tag 5 (optional)
    ---
    # (Main Question, write your main question here.)

    ## Short Answer

    Here you write a paragraph that answers the main question, or a small summary on what this page is about. This should be preferably smaller than 65 words. (Check guidelines as of why)

    ## Longer Answer (optional, make sure to delete these brackets before pushing)

    Here you can write a longer answer or the full details about the topic.

    ## Extra Info (optional, make sure to delete these brackets before pushing)

    Here you can write any extra information about the topic

    ## See Also (optional, make sure to delete these brackets before pushing)

    Here you can indicate other websites or pages
    ```

??? info "Other Pages Template"

    ```md
    ---
    tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Main tag # this tag should be unique, consisting of the smallest amount of words possible.
    - secondary tag 1 (optional)
    - secondary tag 2 (optional)
    - secondary tag 3 (optional)
    - secondary tag 4 (optional)
    - Secondary tag 5 (optional)
    ---
    # (Page title, write your page title here)

    ## Summary

    Here you write a paragraph that answers the main question, or a small summary on what this page is about. This should be preferably smaller than 65 words. (Check guidelines as of why)

    ## main topic

    Here you can write a longer answer or the full details about the topic.

    ## Extra Info (optional, make sure to delete these brackets before pushing)

    Here you can write any extra information about the topic

    ## See Also (optional, make sure to delete these brackets before pushing)

    Here you can indicate other websites or pages
    ```
