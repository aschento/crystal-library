---
tags:
    - Free Trial
    - Party
    - FC
    - Market Board
---
# What are the free trial restrictions?

## Short Answer

The free trial has a leveling cap of level 60, max of 300.000 gill, no PvP access (for now), and no access to social aspects, such as inviting to party, adding friends, joining a {{ FC }} and Market Board.

To add, there is **NO** time limit on the free trial!

## Longer Answer

The free trial is mostly restricted in social aspects and others such as:

- You can **not** send tells/whispers
- You can **not** add friends. (You can be added by someone who has the full game)
- You can **not** join a {{ FC }}.
- You can **not** invite other players to a party (You can be invited to a party by someone who has the full game)
- You can **not** use the Market Board
- You can **not** do PvP content (for now)
- Gill cap is 300k.

And various others

What you can do however:

- Play all the content that is included in the base game ARR (A Realm Reborn) and the *critically acclaimed* expansion heavensward.
- Level all base game and Heavensward expansion jobs to level 60.
- Enjoy some great music and amazing story!
- ***No time limit on the free trial***

For a quicker rundown you can watch Zeplas video here:

{{yt("wjMtI6Ied0M")}}

You can find the official restrictions list here: [Square Enix Support Center](https://support.na.square-enix.com/rule.php?id=5382&tag=freetrial)
