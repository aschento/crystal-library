---
tags:
    - Complete Edition
    - Free Trial
    - Starter Edition
    - Shadowbringers
    - Purchase
---
# What do I buy if I am on the free trial?

## Short Answer

You buy the Complete edition! If you are on steam free trial, buy it on steam, if you are on the standalone/SE Free trial, you can choose either steam or SE Store.

## Long Answer

!!! warning "Reminder!"
    The moment you buy the full game either on Steam or on the Square Enix Store, you will need to buy all future expansion on that respective store. No mix and match between steam and Square Enix versions.

The complete edition is a bundle which includes:

- The Starter Edition
  - ARR & HW Content
  - 30 days of "free" Sub time
- The latest expansion (Shadowbringers)
  - SB & ShB Content

(When Endwalker releases the latest expansion will be Endwalker, and include ShB, and SB)

Check the Long Answer for more information on where to buy the game.

You can find more information on where to buy it at: <https://eu.finalfantasyxiv.com/product/> (Make sure to select the correct country on the top right)

## Extra Info

You can find more info on the differences between the Steam and Square Enix Store versions here:  [What are the differences between steam and SE Store versions](Difference-between-Entry-and-standard-subscription.md)
