---
tags:
    - Versions
    - Steam
    - SE Store
---
# What is the difference between Entry and Standard Subscription

## Short Answer

Outside of the price, the entry subscription allows for only 1 characte per server and 8 total characters, while standard allows for 8 per server and 40 total.

## Long Answer

Entry subscription:

- 1 Character per server
- 8 total characters
- Cheaper, but only 30 days lengths

Standard subscription:

- 8 characters per server
- 40 total characters
- Can be bought in 30,90,or 180 days in subscription length. (with discount the more days you buy, 180 days being equal to entry subscription)

*There is no need for the Standard Tier, as the whole game can be done on 1 character.*

## Extra Info

The price for each tier can be found on the product pages:

![Subscription Price Dollars $ ](Images/SubscriptionPricePoundsEuros.png)
EU (£/€): [https://eu.finalfantasyxiv.com/product/](https://eu.finalfantasyxiv.com/product/)

![Subscription Price Dollars $ ](Images/SubscriptionPriceDollars.png)
NA ($): [https://na.finalfantasyxiv.com/product/](https://na.finalfantasyxiv.com/product/)
