---
tags:
    - Server Pick
    - Preferred World
---
# Which server/world should I pick?

## Short Answer

Any server on a datacenter from your region works. All the content in-game can be done cross-world, and soon in Endwalker across Datacenter as well!

If you pick a preferred world, you will get bonus xp buff up to level 70.

## Longer Answer

As almost all content can be done cross-server, it doesn't matter which server you pick. The only limitations would be joining a {{FC}}, and marrying someone else.
There are a couple noteworthy servers for specific communities:

- Balmung & Mateus - Role Play
- Moogle - French community
- Shiva - German Community

## World Classifications

New World: A recently added World. Players who transfer to or create new characters on designated New Worlds will receive special bonuses.
Congested World: A crowded, highly populated World. Cannot be selected for new character creation or Home World Transfer destinations.
Standard World: Inhabited by a manageable number of player characters.
Preferred World: Relatively sparsely populated World with room to spare. Players who transfer to or create new characters on these Worlds will receive special bonuses.

For a full explanation of the world classifications check here: [https://na.finalfantasyxiv.com/lodestone/playguide/option_service/world_transfer_service/population_balancing/#worldtype](https://na.finalfantasyxiv.com/lodestone/playguide/option_service/world_transfer_service/population_balancing/#worldtype)

### Preferred world bonuses

- Road to 70 buff - Double EXP bonus up to level 70
- Gift of 10 silver chocobo feathers, exchangeable for low-mid level gear to improve the leveling experience
- Gift of 15 days of free play time once per account when getting to level 30 in any class
