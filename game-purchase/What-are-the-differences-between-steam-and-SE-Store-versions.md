---
tags:
    - Versions
    - Steam
    - SE Store
---
# What are the differences between the Steam and SE Store versions?

## Short Answer

The only differences is the addition of steam wallet as an additional payment option when buying it through steam.

## Longer Answer (optional)

The Steam and SE Store versions are the same game, only differing in payment options, and regions availability. The versions are cross-play with each other

Steam allows the user to pay for their subscription, or cash shop items using the steam wallet.

It's also made avaialbe in more regions than the SE Store version.

!!! danger "Carefull"
    Make sure that if you buy into one or the other, you will always need to buy any expansion in the future on the respective store.
