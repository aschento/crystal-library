# Game Purchases and Subscriptions info

Do you have any questions around the free trial or buying the game? Check the pages in this category!

## Most often asked questions

* [Differences between Steam and SE Store](Difference-between-Entry-and-standard-subscription.md)
* [How do I buy the game?](Free-trial-want-to-buy-the-game.md)
