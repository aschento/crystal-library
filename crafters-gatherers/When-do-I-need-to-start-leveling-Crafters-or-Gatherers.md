---
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Level Crafters Gatherers
    - Crafting
    - Gathering
---
# When do I need to start leveling Crafters or Gatherers?

## Short Answer

Whenever you want to! There is no pressure to level crafters, as its NOT necessary to be relevant in end-game content.