# Crafters and Gatherers

In this category you can find all information regarding FFXIV Crafting & Gathering, including how to unlock, questions, and general gameplay content.

## Most often asked questions

* [When do I need to start leveling crafters or gatherers?](When-do-I-need-to-start-leveling-Crafters-or-Gatherers.md)
