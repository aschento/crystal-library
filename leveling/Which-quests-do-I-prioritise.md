---
tags:
    - Quests
    - Leveling
---
# Which quests do I prioritise?

## Short Answer

The priority of quest types is:

1. Job Quests
2. Main Story Quests
3. Blue Quests
4. Normal Quests (safe to ignore forever if you want to)

## Long Answer

The job quests are the most important quests to do, as they will give you:

- Gear
- New Skills

The {{ MSQ }} is the most important quest line in the game, as all content is locked behind it. Most of the times if someone is telling you when you can unlock something, they are referring to the {{ MSQ }} Quest level.

The blue quests are responsible for unlocking all the side-content in the game. This includes, but is not limited by:

- Job Quests
- Unlocking dungeons
- Unlocking Alliance Raids
- Unlocking Trial Series for each expansion
- Unlocking Normal Raids series for each expansion
- Unlocking Materia
- Unlocking Treasure maps
- and much more.
