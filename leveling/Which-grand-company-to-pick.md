---
tags:
    - GC
    - Leveling
---
# Which Grand Company should I pick?

## Short Answer

It doesn't matter! The biggest differences are colour for glamours and headquarter city. You can swap at any time as well.

## Longer Answer

Each grand company has its own headquarters, they differ at:

- Main city
- Main Colour, and thus glamour
- Frontline PvP mount

### Immortal Flames

Main city: Ul'dah
Main Colour: Black

### The order of the Twin Adders

Main city: Gridania
Main Colour: Yellow

### The Maelstrom

Main city: Limsa Lominsa
Main Colour: Red

![Grand Company Gear](images/grand-company-gear.png)
