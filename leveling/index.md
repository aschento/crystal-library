---
tags:
    - Leveling
---
# Leveling

Have questions regarding anything during your leveling process? Check the questions in this category!

## Most often asked questions

- [When do I get my first Mount?](When-do-I-get-my-first-mount.md)
