---
tags:
    - Unlock Flying
    - Leveling
---
# When do I unlock flying?

## Short Answer

The first time you unlock flying is when you have finished the quest "The Ultimate Weapon", which is done at the end of base {{ARR}} (not including patches). This will unlock flying in all {{ ARR }} zones.

Starting from {{ HW }}, you will need to obtain and find all aether currents in each zone to unlock.
