---
tags:
    - lvl49 Quest
---
# My next quest is level 49, what do I do?

## Short Answer

If you are stuck with your level you can do the following:

- Duty Roulette: Leveling (only once a day)
- Unlock Dzameal Darkhold Side-dungeon (Font of fear)
- Unlock Aurum Vale side-dungeon (Going for Gold in Vesper Bay)

You can find links to the dungeon unlock quests in the Extra Info.

## Extra Info

- [https://ffxiv.gamerescape.com/wiki/Fort_of_Fear](https://ffxiv.gamerescape.com/wiki/Fort_of_Fear)
- [https://ffxiv.gamerescape.com/wiki/Going_for_Gold](https://ffxiv.gamerescape.com/wiki/Going_for_Gold)
