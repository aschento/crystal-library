---
description: Here you can find a lot of questions answered about the game, ranging from How to buy the game, to when do I get a mount!
hide:
    - navigation
    - toc
---

# Crystal Library FFXIV

Welcome to the Crystal Library! A wonderful place with answers to all (well almost) your questions regarding the game. Ranging from how to unlock other classes, tips & tricks, where to find the best guides, how do I buy the game, and much more!

All content aims to easily explain or link to the necessary proper guides that wonderful people in this community made just for you!

Use the tabs above, or the search bar to search for your question. You can also use the [Tags](tags.md) page to look at all questions that can be related to each other!

Still can't find an answer to your question? Join ZeplaHQ's discord and I (Robin Naite), or some other wonderful player will help you out! Join here: [https://discord.gg/zeplahq](https://discord.gg/zeplahq)

## Most often asked questions

* [I am Level 30 why no job advancements?](battle-jobs/I-am-level-30-why-no-job-advancement-quest.md)
* [When do I get my first mount?](leveling/When-do-I-get-my-first-mount.md)

## About the project

The Crystal Library is a project grown out of the constant repetitive nature of answering questions in the #sprout_help channel of ZeplaHQ discord (which you can find [here](https://discord.gg/zeplahq)) by myself (Robin Naite). The aim is to continuously extend this knowledge base with information or links to the information regarding the game, and all the question new players, or people new to specific type of content.
The FFXIV community has made some wonderful content over the years, but its all spread over various discord servers, websites, google docs and Youtube videos. On this website I (Robin Naite) will aim to link to all this amazing information for easier searching.
  